temperaturesC = []
temperaturesF = []

for i in range(24):
    print("This is for hour",[i])
    averageTempHour = float(input("Please enter the average temperature for this hour in centigrade: "))

    temperaturesC.append(averageTempHour)

    fahrenheit = ((averageTempHour) * (1.8) + 32)
    temperaturesF.append(fahrenheit)

temperaturesC.sort()
temperaturesF.sort()
print("Max temp in C was:",temperaturesC[-1:])
print("Min temp in C was:",temperaturesC[0])

averageTempC = sum(temperaturesC)/24
round(averageTempC,1)
print("The average temperature in C was",averageTempC)

print("Max temp in F was:",temperaturesF[-1:])
print("Min temp in F was:",temperaturesF[0])

averageTempF = sum(temperaturesF)/24
round(averageTempF,1)
print("The average temperature in F was",averageTempF)
