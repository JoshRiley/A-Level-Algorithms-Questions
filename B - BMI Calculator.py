students = []
bmiFinal = []
bmiRating = []

for i in range(30):
    studentID = input("Please enter the student's identification number: ")
    studentHeight = float(input("Please enter the height of the student in metres: "))
    studentWeight = float(input("Please enter the weight of the student in KG: "))

    bmi = (studentWeight)/((studentHeight)*(studentHeight))

    bmi = round(bmi,2)
    print(bmi)

    if bmi > 25:
        student = "OVERWEIGHT"
    elif bmi >=19 and bmi <= 25:
        student = "NORMAL"
    elif bmi < 19:
        student = "UNDERWEIGHT"

    students.append(studentID)
    bmiFinal.append(bmi)
    bmiRating.append(student)

for i in range(len(students)):
    print("Student Number",(students[i]),"has a BMI of",(bmiFinal[i]),"and is",(bmiRating[i]))